var mymap = L.map('mapid').setView([-34.57390275881517, -58.642705259924995], 14);

var marker = L.marker([-34.57390275881517, -58.642705259924995]).addTo(mymap);

marker.bindPopup("<b>Hurling club</b><br>Mistica").openPopup();

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
}).addTo(mymap);
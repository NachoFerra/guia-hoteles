$(function(){
    
    		$("[data-toggle='tooltip']").tooltip();
    		$("[data-toggle='popover']").popover();
    		$('.carousel').carousel({
    			interval: 2000
    		});

    		$('#contacto').on('show.bs.modal', function(e){
    			console.log('el modal se esta mostrando');
    			$('#contactoBtn').removeClass('btn-outline-success');
    			$('#contactoBtn').addClass('btn-danger');
    			$('#contactoBtn').prop('disabled',true);
    		});

    		$('#contacto').on('hidden.bs.modal', function(e){
    			console.log('el modal se esta oculto');
    			$('#contactoBtn').removeClass('btn-danger');
    			$('#contactoBtn').addClass('btn-outline-success');
    			$('#contactoBtn').prop('disabled',false);
    		});
    	});
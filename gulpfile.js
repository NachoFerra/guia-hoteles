'use strict'

var gulp = require('gulp'),
	sass = require('gulp-sass');

gulp.task('sass', function(done){
	gulp.src('./css/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./css'));
done();
});

gulp.task('sass:watch',function(done){
	gulp.watch('./css/*.scss',['sass']);
	done();
});

gulp.task('browser-sync', function(done) {
	var files = ['./*.html','./css/*.css','./img/*.{ png, jpg, gif}','./js/*.js']
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
    done();
});

gulp.task('default',['browser-sync'],function(done){
	gulp.start('sass:watch');
	done();
});